package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Proveedor;
import com.example.demo.service.ProveedorServiceImpl;


@RestController
@RequestMapping("/api")
public class ProveedorController {

	@Autowired
	ProveedorServiceImpl proveedorService;
	
	@GetMapping("/proveedor")
	public List<Proveedor> listarProveedor(){
		return proveedorService.listarProveedores();
	}
	
	
	@PostMapping("/proveedor")
	public Proveedor salvarProveedor(@RequestBody Proveedor proveedor) {
		
		return proveedorService.guardarProveedor(proveedor);
	}
	
	
	@GetMapping("/proveedor/{id}")
	public Proveedor cursoXID(@PathVariable(name="id") String id) {
		
		Proveedor Curso_xid= new Proveedor();
		
		Curso_xid=proveedorService.proveedorXID(id);
		
		System.out.println("Proveedor XID: "+Curso_xid);
		
		return Curso_xid;
	}
	
	@PutMapping("/proveedor/{id}")
	public Proveedor actualizarCurso(@PathVariable(name="id")String id,@RequestBody Proveedor proveedor) {
		
		Proveedor Curso_seleccionado= new Proveedor();
		Proveedor Curso_actualizado= new Proveedor();
		
		Curso_seleccionado= proveedorService.proveedorXID(id);
		
		Curso_seleccionado.setNombre(proveedor.getNombre());
		
		Curso_actualizado = proveedorService.actualizarProveedor(proveedor);
		
		System.out.println("El proveedor actualizado es: "+ Curso_actualizado);
		
		return Curso_actualizado;
	}
	
	@DeleteMapping("/proveedor/{id}")
	public void eleiminarCurso(@PathVariable(name="id")String id) {
		proveedorService.eliminarProveedor(id);
	}
	
}
