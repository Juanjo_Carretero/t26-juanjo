package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ICientificoDAO;
import com.example.demo.dto.Cientifico;
import com.example.demo.dto.Proyecto;

@Service
public class CientificoServiceImpl implements ICientificoService{

	@Autowired
	ICientificoDAO CientificoDAO;
	
	
	@Override
	public List<Cientifico> listarCientifico() {
		return CientificoDAO.findAll();
	}

	@Override
	public Cientifico guardarCientifico(Cientifico cientifico) {
		return CientificoDAO.save(cientifico);
	}

	@Override
	public Cientifico CientificoXDNI(String dni) {
		return CientificoDAO.findById(dni).get();
	}

	@Override
	public Cientifico actualizarCientifico(Cientifico cientifico) {
		return CientificoDAO.save(cientifico);
	}

	@Override
	public void eliminarCientifico(String dni) {
		CientificoDAO.deleteById(dni);
		
	}

}
