package com.example.demo.service;

import java.util.List;


import com.example.demo.dto.Cientifico;



public interface ICientificoService {

	
	//Metodos del CRUD
			public List<Cientifico> listarCientifico(); //Listar All 
			
			public Cientifico guardarCientifico(Cientifico cientifico);	//Guarda un Curso CREATE
			
			public Cientifico CientificoXDNI(String dni); //Leer datos de un Curso READ
			
			public Cientifico actualizarCientifico(Cientifico cientifico); //Actualiza datos del Curso UPDATE
			
			public void eliminarCientifico(String dni);// Elimina el Curso DELETE
}
