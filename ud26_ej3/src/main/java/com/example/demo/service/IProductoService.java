package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Productos;


public interface IProductoService {

	//Metodos del CRUD
			public List<Productos> listarProductos(); //Listar All 
			
			public Productos guardarProductos(Productos productos);	//Guarda un Curso CREATE
			
			public Productos productosXCodigo(int codigo); //Leer datos de un Curso READ
			
			public Productos actualizarProductos(Productos productos); //Actualiza datos del Curso UPDATE
			
			public void eliminarProductos(int codigo);// Elimina el Curso DELETE
	
}
